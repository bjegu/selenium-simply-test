const {Builder, By, Key, until} = require('selenium-webdriver');



test('For this calculator: 9*9 should return 81', async () => {
    // creating a driver
    const driver = new Builder().forBrowser('chrome').build();
    // get a page
    await driver.get('http://www.anaesthetist.com/mnm/javascript/calc.htm');
    await (await driver.findElement({ name: 'nine' })).click();
    await (await driver.findElement({ name: 'mul' })).click();
    await (await driver.findElement({ name: 'nine' })).click();
    await (await driver.findElement({ name: 'result' })).click();
    const result = await (await (await (await driver).findElement(By.name("Display"))).getAttribute("value"))
    expect(result).toEqual("81");
});

test('For this calculator: 1*7 should return 7', async () => {
    // creating a driver
    const driver = new Builder().forBrowser('chrome').build();
    // get a page
    await driver.get('http://www.anaesthetist.com/mnm/javascript/calc.htm');
    await (await driver.findElement({ name: 'one' })).click();
    await (await driver.findElement({ name: 'mul' })).click();
    await (await driver.findElement({ name: 'seven' })).click();
    await (await driver.findElement({ name: 'result' })).click();
    const result = await (await (await (await driver).findElement(By.name("Display"))).getAttribute("value"))
    expect(result).toEqual("7");
});